import argparse
from time import sleep
import traceback
import os
from os import rename, listdir
import logging
from pathlib import Path
import sys

from selenium import webdriver
from selenium.common.exceptions import InvalidArgumentException
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By


def get_logger() -> logging.Logger:
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    handler = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s [%(levelname)s] %(message)s")
    handler.setFormatter(formatter)

    logger.addHandler(handler)
    return logger


def parse_args() -> argparse.Namespace:
    program_name = "main.py"
    description="Program fixes subtitles encoding." \
        "Subtitles have number in file name. It means the series number. " \
        "Specify range to encode (first and last number of the series)."

    parser = argparse.ArgumentParser(prog=program_name, description=description)
    parser.add_argument("-f", "--first", action="store", type=int, required=True,
                        help="first number of the series")
    parser.add_argument("-l", "--last", action="store", type=int, required=True,
                        help="last number of the series")

    return parser.parse_args()


def download_file(logger: logging.Logger, driver: webdriver, input_file: str) -> None:
    url = "https://subtitletools.com/convert-text-files-to-utf8-online"
    driver.get(url) 

    # upload input file
    file_input = driver.find_element(By.CSS_SELECTOR, "input[type='file']")
    file_input.send_keys(input_file)
    logger.info(f"File '{input_file}' uploaded")

    driver.find_element(By.CSS_SELECTOR, "button[type='submit']").click()
    sleep(1)

    # download result
    driver.find_element(By.CLASS_NAME, "a-text2").click()
    logger.info(f"Downloading started")
    sleep(1)


# FIXME: rename file_id
def rename_file(logger: logging.Logger, file_id: int, input_file_name: str, output_file_pattern: str) -> None:
    output_file_name = output_file_pattern.format(str(file_id))
    logger.info(f"Renaming file '{input_file_name}' to '{output_file_name}'")

    rename(input_file_name, output_file_name)
    logger.info(f"File '{input_file_name}' successfully renamed")


if __name__ == "__main__":
    driver = None
    try:
        logger = get_logger()
        args = parse_args()

        first_file = args.first
        last_file = args.last

        downloads_directory = os.path.join(str(Path.home()), "Downloads")
        subtitles_directory = os.path.join(downloads_directory, "[Bleach][1-113][R2DVDRIP][x264_ac3]")
        subtitle_filename_pattern = "(SB-RAW)Bleach {} (R2DVD 960x728 x264 AC3).srt"

        path_elements = [subtitles_directory, subtitle_filename_pattern]
        source_file_pattern = os.path.join(*path_elements)
        result_file_pattern = os.path.join(*path_elements)

        options = Options()
        options.add_argument("--headless")
        driver = webdriver.Firefox(options=options)

        for file_id in range(first_file, last_file):
            files_before_downloading = listdir(downloads_directory)
            download_file(logger, driver, source_file_pattern.format(str(file_id)))
            files_after_downloading = listdir(downloads_directory)

            # calculate difference between two lists
            new_files = list(
                set(files_after_downloading) - set(files_before_downloading)
            )
            input_file_name = os.path.join(downloads_directory, str(new_files[0]))
            logger.info(f"File '{input_file_name}' successfully downloaded")

            rename_file(logger, file_id, input_file_name, result_file_pattern)

    except KeyboardInterrupt:
        print("\r", end="") # remove ^C symbol
        logger.info("Stopped")
    except InvalidArgumentException as e:
        logger.error(e.msg)
    except Exception as e:
        logger.error(e)
        logger.error(traceback.print_tb(e.__traceback__))
    finally:
        if driver is not None:
            driver.quit()

